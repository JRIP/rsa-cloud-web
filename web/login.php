<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Login</title>
    <link rel="icon"  href="images/logo.ico">
    <link rel="stylesheet" href="css/login.css">
  </head>
  <body>
      <?php

        session_start();

        if(!isset($_SESSION["user_id"])){
            include "menu.html";
        } else {
            include "menu_loged.html";
        }
    ?>

    <div class="containerLogin">
      <div class="cloud"><a href="https://www.rsacloud.tk/owncloud">cloud</a></div>
      <div class="stream"><a href="inicio_public.php">stream</a></div>
    </div>

  </body>
</html>
