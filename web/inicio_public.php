<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>RSA Streaming</title>
    <link rel="icon"  href="images/logo.ico">

    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

    <!--Archivo de bootstrap-->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <!--Importamos iconos-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    <?php

        session_start();

        //Comprobamos que la sesión esté abierta para poder acceder a la página
        if(!isset($_SESSION["user_id"])){
            header("Location:sesiones/login.php");
        }
    ?>


    <style>

        *{
            box-sizing: border-box;
            outline:none;
            margin: 0;
        }

        body{
            margin: 0;
            font-family: Arial, Helvetica, sans-serif;

            /*background: url(images/green.png) no-repeat center center fixed;
            background-size: cover;*/

            background-color:#2F322E;

        }

        #mensaje_alerta{
            display:none;
        }


        /*Menú de navegación*/

        .navbar-default{
            margin:0;
            height:7.5vh;
	    border:none;
            font-weight: 900;
        }

        .navbar{
            border-radius: 0px;
        }


        .navbar-header a {
            float: left;
            display: inline-block;
            text-decoration: none;
            font-size: 20px;
            font-family: arial;
            color: #1e1e1e;
            margin: 25px 0 0 20px;
            padding: 0;
        }

        .navbar-right{
            margin-top:10px;
        }

        .navbar-header img {
            float: left;
            width: 100px;
            margin: 0;
        }

        #logo{
            width:60px;
            height:60px;
        }

        .navbar-toggle{
            margin-top:20px;
        }

        ul{
            list-style:none;
            padding:0;
        }
        li {
          text-align: center;
          font-weight: 900px;
        }


        /*estilo de los botones de playlist*/

        .paginas{
            cursor:pointer;
            display:block;
            color:#fff;
            font-size: 20px;
            padding:15px;
            text-decoration:none;
        }

        .paginas:link{
            cursor:pointer;
            color:#fff;
            text-decoration:none;
        }

        .paginas:visited{
            cursor:pointer;
            color:#fff;
            text-decoration:none;
        }


        .paginas:active{
            cursor:pointer;
            color:#fff;
            text-decoration:none;
        }

        a{
            cursor:pointer;
        }

        #pagina_actual{
            border-left: 5px solid green;
        }


        .row.main-container{
            height:74.5vh;
        }

        #contenedor_1{
            background-color:black;
            overflow:auto;
            height:100%;
            padding:0;
            margin:0;
            color:#fff;
        }

        #contenedor_2{
            padding:0;
            height:100%;
            margin:0;
        }

        h2{
            margin:0;
            color:#fff;
            height: 5%;
            background-color: rgba(90, 89, 89, 0.719);
            text-align: center;
        }



        /*TABLA*/


        #contenedor_tabla{
            background-color: #393936;
            margin:auto;
            height:95%;
            overflow:auto;
            width: 100%;

        }

        table{
            margin: 0;
            width: 100%;
            font-size: 20px;

        }

        tr{
            cursor:pointer;
            background-color: rgba(255, 255, 255, 0.0);

        }

        tr:hover{
            background-color: #453030);
        }

        td{
            text-align: left;
            color: #fff;
            padding:10px;
        }


        .play_list{
            color: #fff;
            font-size:20px;
            border:none;
            background-color: rgba(0, 0, 0, 0);
            cursor:pointer;
            text-align:center;
            outline:none;
        }


        .play_list:hover{
            color: black;
        }




        /*Elementos del reproductor*/

        #contenedor{
            position: fixed;
            padding: 0;
            height:18vh;
            bottom:0%;
            width: 100%;
            background-color:#302f2f67;

        }

        #contenedor_imagen{
            /*display:none;*/
            visibility:hidden;
        }

        #contenedor_player_nav{
            display:none;
        }

        #minimized{
            display:none;
        }

        #track{
            position: relative;
            margin: auto;
            margin-top: 20px;
            margin-bottom: 0;
            width: 90%;
        }

        #reproduccion {
            position: relative;
            margin:0;
            width: 100%;
            height: 5px;
            background-color: #A6A7A9;
            border-radius: 8px;
            cursor: pointer;
        }
        #progreso {
            float: left;
            height: 5px;
            width: 0%;
            border-radius: 8px;
            background: #00CC99;
            cursor: pointer;
        }

        #puntero {
            position: absolute;
            width: 20px;
            height: 20px;
            background-color: #00CC99;
            top: 50%;
            left: 0%;
            margin-left: -10px;
            margin-top: -10px;
            border-radius: 50%;
            cursor: pointer;
            z-index: 5;
        }

        #time{
            overflow:hidden;
        }

        .tiempo{
            display: inline-block;
            color:#fff;
            width: 50%;
            margin:0;
            margin-top: 10px;
        }

        #tiempo_actual{
            text-align:left;
        }

        #tiempo_total{
            text-align:right;
            float: right;
        }


        #controles{
            position: relative;
            padding: 10px;
        }


        .botones_control{
            position: relative;
            width: 50px;
            height: 50px;
            color: black;
            left:42%;
            margin: 10px 0px 10px;
            font-size:20px;
            padding: 0;
            border:none;
            /*background-color: rgba(255, 255, 255, 0);*/
            background-color: #fff;
            cursor:pointer;
            outline:none;
            border-radius: 50%;
        }


        .botones_control:hover {
            background-color: #00CC99;
        }

        #contenedor_lista{
            margin-right:0;
        }


        #pause{
            display:none;
        }


        #informacion{
            display: inline-block;
            position: absolute;
            top: 20%;
            left:2%;
        }

        .info{
            display: block;
            outline: none;
            margin: 0;
            cursor: pointer;
        }

        #nombre{
            font-weight: 800;
            font-size: 20px;
            color: #fff;
        }

        #autor{
            font-weight: 550;
            color: rgba(255, 255, 255, 0.582);
        }


        #sonido{
            display: inline-block;
            position: absolute;
            top: 20%;
            right:2%;
        }

        #random , #repetir{
            position: relative;
            background-color: rgba(255, 255, 255, 0);
            color: black;
            left:42%;
            width: 50px;
            margin: 10px auto;
            font-size:20px;
            cursor:pointer;
            outline:none;
            border:none;
        }


        @media (max-width:768px){
            #playlist_mini{
                display:block;
            }
        }

        /*Estilo de altura en portátiles medianos*/
        @media (max-width:1440px){

            #contenedor_1{
                display:none;
            }
            #contenedor_2{
                width:100%;
            }

            .navbar-default{
                margin:0;
                height:9vh;
                border:none;
            }

            h2{
                height: 5vh;
            }

            .row.main-container {
                height: 64vh;
            }

            #contenedor{
                height:22vh;

            }


        }




        @media (max-width:480px){

            body{
                background-image:none;
                background-color:black;
            }

            .navbar-default{
                height:14%;
            }

            #playlist_mini{
                display:block;
            }

            h2{
                height:5vh;
            }

            #contenedor_principal {
                height: 90vh;
            }

            .row.main-container {
                height: 78vh;
            }

            #contenedor_2{
                position:relative;
                height:100%;
            }

            #contenedor_tabla{
                height:100%;
            }

            table{
                font-size:15px;

            }

            #minimized{
                display:flex;
                align-items: center;
                position:fixed;
                height:8vh;
                color:#fff;
                width:100%;
                bottom:0;
                background-color: #29292D;
            }


            #boton_subir{
                display:inline-block;
                background-color: rgba(255, 255, 255, 0);
                border:none;
                margin-left:2%;
            }

            #nombres_mini{
                width:90%;
                font-size:16px;
                text-align:center;

            }

            #nombre_3{
                font-weight: 800;
                color: #fff;
            }

            #artista_3{
                font-weight: 550;
                color: rgba(255, 255, 255, 0.582);
            }



            /*Contenedor de player a pantalla completa*/

            #contenedor{
                display:none;
                position:fixed;
                height:100%;
                background-color:#808367;
            }

            #contenedor_player_nav{
                display:block;
                font-size:25px;
                color:#fff;
            }

            #boton_bajar{
                margin-left:20px;
                background-color: rgba(255, 255, 255, 0);
                border:none;
            }

            #controllers{
                position:absolute;
                width:100%;
                bottom:2%;
            }



            #contenedor_imagen{
                visibility: visible;
                display:flex;
                width:75%;
                height:260px;
                margin:auto;
                margin-bottom:35px;
                text-align:center;
                background-color:#BBB88A;
                color:black;
                justify-content: center;
                align-items: center;
            }

            #fondo{
                font-size:200px;

            }


            #nombre_2{
                width:100%;
                margin:auto;
                text-align:center;
                font-size:25px;
                font-weight:bold;
            }

            #informacion{
                display:block;
                position:relative;

            }

            .info{
                display:block;
                outline: none;
                margin: 0;
                cursor: pointer;
            }


            #controles_musica{
                position:relative;
                text-align: center;
            }


            .botones_control{
                position: relative;
                width: 50px;
                height: 50px;
                color: black;
                left:0%;
                margin: 10px 0px 10px;
                font-size:20px;
                padding: 0;
                border:none;
                /*background-color: rgba(255, 255, 255, 0);*/
                background-color: #fff;
                cursor:pointer;
                outline:none;
                border-radius: 50%;
            }


            #stop{
                display:none;
            }

            #play{
                padding-left:4px;
            }

            #play, #pause{
                font-size:30px;
                width: 60px;
                height: 60px;
                margin: 10px 20px 10px;
            }

            #prev, #next{
                color:#fff;
                font-size: 30px;
                background-color: rgba(255, 255, 255, 0);
            }

            #random, #repetir{
                position: static;
                font-size: 20px;
            }

    }


    /*Por si el usuarios usa la pantalla del mobil en landscape*/

    @media (max-width: 823px) and (orientation: landscape){

        *{
            visibility:hidden;
        }

        body{
            background-image: none;
            background-color: #fff;
        }

        #mensaje_alerta{
            display:flex;
            align-items: center;
            visibility:visible;
            position:absolute;
            width:100%;
            height:100vh;
        }

        #imagen_alerta{
            visibility:visible;
            height:50%;
            width:50%;
            margin:auto;
        }
    }

    </style>

</head>
<body>

<div id="mensaje_alerta">
    <img src="images/rotate_device.gif" id="imagen_alerta" alt="Please, rotate your device">
</div>

<nav class="navbar navbar-default">
        <div class="container-fluid">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <img id="logo" src="images/loog.png" alt="Logo">
            <a href="index.php">RSA Cloud</a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

            <ul class="nav navbar-nav navbar-right">
              <li><a href="index.php">Home</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Services <span class="caret"></span></a>
                <ul class="dropdown-menu">
                <li><a href="https://www.rsacloud.tk/owncloud">Cloud Storage</a></li>
                <li><a href="inicio_public.php">Cloud Stream</a></li>
                </ul>
              </li>
              <li><a href="aboutUs.php">About us</a></li>
              <li><a href="contact.php">Contact</a></li>


              <li class="dropdown" id="playlist_mini">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">PlayLists <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="inicio_public.php">Public</a></li>
                    <li><a href="inicio_private.php">Private</a></li>
                </ul>
              </li>


              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user"></span> <?php echo $_SESSION["user_name"];?> <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a data-toggle="modal" data-target="#subir_musica"> Upload music</a></li>
                    <li><a href="sesiones/cerrar_sesion.php">Log out</a></li>
                </ul>
              </li>

            </ul>
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>


      <div class="modal fade" id="subir_musica" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4>New Song</h4>
                </div>

                <div class="modal-body">
                    <form method="post" action="new_music.php" enctype="multipart/form-data">

                        <label for="titulo">Song title * </label>
                        <input type="text" class="form-control" name="titulo" ><br><br>

                        <label for="autor">Song artist * </label>
                        <input type="text" class="form-control" name="autor" ><br><br>

                        <label for="archivo">File * </label>
                        <input type="file" name="archivo" id="archivo"><br><br>

                        <p>* Required field</p>
                        <input type="submit" class="btn btn-primary" name="enviar" class="botones" value="Send">
                        <input type="reset" class="btn btn-primary" name="borrar" class="botones" value="Delete information">
                    </form>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>

            </div>

        </div>

    </div>


    <div id="contenedor_principal" class="container-fluid">
        <div class="row main-container">
            <div id="contenedor_1" class="col-lg-2 hidden-md hidden-xs hidden-sm">
                <h2>Playlists</h2>
                <ul>
                    <li id="pagina_actual"><a class="paginas" href="inicio_public.php">Public</a></li>
                    <li><a class="paginas" href="inicio_private.php">Private</a></li>
                </ul>
            </div>

            <div id="contenedor_2" class=" col-xs-12 col-lg-10">
                <h2 class="col-xs-6">Title</h2>
                <h2 class="col-xs-6">Artist</h2>

                <div id="contenedor_tabla">

                        <?php
                            $servername = "localhost";
                            $username = "root";
                            $password = "root123";
                            $db = "RSA";
                            $tabla = "public_music";

                            // Create connection
                            $conn = new mysqli($servername, $username, $password, $db);

                            // Check connection
                            if ($conn->connect_error) {
                                die("Connection failed: " . $conn->connect_error);
                            }

                            //Extraemos datos de la tabla

                            $sql = "SELECT title, artist, file_path FROM $tabla";

                            $result = $conn->query($sql);

                            if ($result->num_rows > 0) {
                                //Mostramos el resultado

                                echo "<table>";

                                $contador = 0; //Añadimos esta variable para asignar IDs a los elementos de la tabla
                                $url = array(); //Guardaremos las rutas de los archivos de audio
                                $titulos = array();
                                $artistas = array();

                                while($row = $result->fetch_assoc()) {

                                echo "<tr id='fila_$contador'>";
                                echo "<td>" . " <button id='$contador' class='play_list'> <i class='fa fa-play'></i> </button> ". "</td>";
                                echo "<td id='titulo_$contador'>" .  $row["title"] . "</td>";
                                echo "<td id='autor_$contador'>" .  $row["artist"] . "</td>";
                                echo "</tr>";


                                $titulos[$contador] = $row["title"];
                                $artistas[$contador] = $row["artist"];
                                $url[$contador] = $row["file_path"];

                                $contador = $contador + 1;

                                }

                                $longitud = count($url); //Guardamos la longitud del array

                                echo "</table>";
                            } else {
                                echo "Sin resultados";
                            }
                            //Cerramos conexión
                            $conn->close();
                    ?>

                </div>
            </div>
        </div>

    </div>

    <div id="minimized">

        <button id="boton_subir">
            <span class="glyphicon glyphicon-chevron-up"></span>
        </button>

        <div id="nombres_mini">
            <span id="nombre_3">Titulo</span> <span> ·</span>
            <span id="artista_3">Autor</span>
        </div>


    </div>

    <div id="contenedor">

            <div id="contenedor_player_nav">
                <button id="boton_bajar">
                    <span class="glyphicon glyphicon-chevron-down"></span>
                </button>
                <p id="nombre_2"> Titulo</p>
            </div>

        <div id="controllers">

            <div id="contenedor_imagen">
                <span id="fondo" class="glyphicon glyphicon-music"></span>
            </div>

            <div id="track">
                <div id="reproduccion">
                    <div id="progreso"></div>
                    <div id="puntero"></div>
                </div>
                <div id="time">
                    <p id="tiempo_total" class="tiempo">00:00</p>
                    <p id="tiempo_actual" class="tiempo">00:00</p>
                </div>
            </div>

            <div id="controles">

                <div id="informacion">
                    <span id="nombre" class="info">Titulo</span>
                    <span id="autor" class="info">Autor</span>
                </div>

                <div id="controles_musica">
                    <button id="random">
                        <span class="glyphicon glyphicon-random"></span>
                    </button>

                    <button id="prev" class="botones_control">
                        <i class="fa fa-step-backward"></i>
                    </button>

                    <button id="play" class="botones_control">
                        <i class="fa fa-play"></i>
                    </button>

                    <button id="pause" class="botones_control">
                        <i class="fa fa-pause"></i>
                    </button>

                    <button id="stop" class="botones_control">
                        <i class="fa fa-stop"></i>
                    </button>

                    <button id="next" class="botones_control">
                        <i class="fa fa-step-forward"></i>
                    </button>

                    <button id="repetir">
                        <span class="glyphicon glyphicon-retweet"></span>
                    </button>


                </div>
            </div>
        </div>
    </div>

    <script src="js/jquery.js"></script>

    <!--Archivo de bootstrap-->
    <script src="js/bootstrap.min.js"></script>

    <script>

    $(document).ready(function(){

        //VARIABLES

        var cancion;
        var holding = false;
        var musica_actual = 0;
        var nombre_cancion = document.getElementById("nombre").innerText;
        var autor = document.getElementById("autor").innerText;
        var duracion;
        var minutos;
        var segundos;
        var num_aleatorio;
        var canciones_reproducidas = [];
        var numero;
        var maximo;
        var minimo;
        var comprobar_num;
        var salir; //Variable para bucle while
        var random_activo = false;
        var longitud_array = <?php echo $longitud;?>;
        var i; //Variable para bucles
        var titulos = <?php echo json_encode($titulos);?>;
        var artistas = <?php echo json_encode($artistas);?>;
        var rutas = <?php echo json_encode($url);?>;

        //Añadimos el evento init

        window.addEventListener('load', init(), false);

        function init() {
            audio = new Audio();
            audio.loop = false;
            audio.src = rutas[musica_actual];

            //Mostramos el titulo de la canción y autor
            actualizar_info();

        }

        audio.addEventListener('timeupdate', updateTrack, false);
        audio.addEventListener('loadedmetadata', function () {
            duracion = this.duration;

            //Mostramos el tiempo de duración de la canción
            time = audio.duration;
            console.log(time);
            minutos = Math.floor( time / 60 );
            segundos = Math.floor(time % 60);

            //Anteponiendo un 0 a los minutos si son menos de 10
            minutos = minutos < 10 ? '0' + minutos : minutos;

            //Anteponiendo un 0 a los segundos si son menos de 10
            segundos = segundos < 10 ? '0' + segundos : segundos;

            result = minutos + ":" + segundos;

            $("#tiempo_total").text(result);

        }, false);

        //Especificamos lo que pasa cuando una canción se acabe

        audio.addEventListener('ended', function () {

            console.log(longitud_array);
            console.log(canciones_reproducidas.length);

                if (canciones_reproducidas.length < longitud_array){

                    if (random_activo == false){

                    if (audio.ended == true){
                        musica_actual = (musica_actual + 1);

                        audio.src = rutas[musica_actual];
                        audio.play();
                        $("#play").hide();
                        $("#pause").show();

                        //Actualizamos los datos
                        actualizar_info();
                    }

                } else {

                        minimo = 0;
                        maximo = longitud_array;

                        salir = "no";

                        while (salir == "no"){

                            //Hago cosas

                            num_aleatorio = Math.floor(Math.random()*(maximo-minimo))+minimo;
                            numero = canciones_reproducidas.indexOf(num_aleatorio); //Retornará -1 si no se encuentra en el array

                            //Comprobamos si debemos salir del bucle

                            if( numero < 0 ){ //No se encuentra en el array
                                salir = "si";
                            }
                        }

                        if (audio.ended == true){

                            console.log(num_aleatorio);

                            musica_actual = num_aleatorio;

                            audio.src = rutas[musica_actual];
                            audio.play();
                            $("#play").hide();
                            $("#pause").show();

                            //Actualizamos los datos
                            actualizar_info();
                        }
                    }


                }



        }, false);

        window.onmousemove = function (e) {
            e.preventDefault();
            if (holding) seekTrack(e);
        }
        window.onmouseup = function (e) {
            holding = false;
            console.log(holding);
        }
        reproduccion.onmousedown = function (e) {
            holding = true;
            seekTrack(e);
            console.log(holding);
        }

        function updateTrack() {
            curtime = audio.currentTime;
            percent = Math.round((curtime * 100) / duracion);
            progreso.style.width = percent + '%';
            puntero.style.left = percent + '%';

            /*INFORMACIÓN DEL TIEMPO DE REPRODUCCION*/

            time = audio.currentTime;
            minutos = Math.floor( time / 60 );
            segundos = Math.floor(time % 60);

            //Anteponiendo un 0 a los minutos si son menos de 10
            minutos = minutos < 10 ? '0' + minutos : minutos;

            //Anteponiendo un 0 a los segundos si son menos de 10
            segundos = segundos < 10 ? '0' + segundos : segundos;

            var resultado = minutos + ":" + segundos;

            $("#tiempo_actual").text(resultado);
        }


        function actualizar_info(){

            comprobar_num = canciones_reproducidas.indexOf(musica_actual);

            if (comprobar_num < 0){

                canciones_reproducidas.push(musica_actual);
            }

            document.getElementById("nombre").innerHTML = titulos[musica_actual];
            document.getElementById("nombre_2").innerHTML = titulos[musica_actual];
            document.getElementById("nombre_3").innerHTML = titulos[musica_actual];


            document.getElementById("autor").innerHTML = artistas[musica_actual];
            document.getElementById("artista_3").innerHTML = artistas[musica_actual];


            for (i=0; i < longitud_array; i++){
                document.getElementById("fila_" + i).style.backgroundColor='rgba(255, 255, 255, 0.0)';
            }

            document.getElementById("fila_" + musica_actual).style.backgroundColor='#772C2C';

        }

        function seekTrack(e) {
            event = e || window.event;
            var x = e.pageX - track.offsetLeft - reproduccion.offsetLeft;
            percent = Math.round((x * 100) / track.offsetWidth);
            if (percent > 100) percent = 100;
            if (percent < 0) percent = 0;
            progreso.style.width = percent + '%';
            puntero.style.left = percent + '%';
            audio.play();

            $("#play").hide();
            $("#pause").show();

            audio.currentTime = (percent * duracion) / 100
        }

        //Eventos

        $("#boton_subir").click(function(){

            $("#contenedor").show();

        });

        $("#boton_bajar").click(function(){

            $("#contenedor").hide();

        });


        //Reproducir
        $("#play").click(function(){
            audio.play();

            $("#play").hide();
            $("#pause").show();

        });

        $(".play_list").click(function(){

            musica_actual = this.id;
            musica_actual = musica_actual % (longitud_array);
            audio.src = rutas[musica_actual];
            audio.play();

            $("#play").hide();
            $("#pause").show();

            actualizar_info();
        });

        //Pausar la canción
        $("#pause").click(function(){
            audio.pause();

            $("#pause").hide();
            $("#play").show();

        });

        //Parar la reproducción
        $("#stop").click(function(){
            audio.load();

            $("#pause").hide();
            $("#play").show();
        });

        //Reproducir la siguiente canción
        $("#next").click(function(){
            musica_actual = (musica_actual + 1);
            musica_actual = musica_actual % (longitud_array);

            audio.src = rutas[musica_actual];
            audio.play();
            $("#play").hide();
            $("#pause").show();

            //Actualizamos los datos
            actualizar_info();
        });


        //Reproducir la canción anterior
        $("#prev").click(function(){

            /*
            Se reproducirá una canción anterior solo si hay
            una canción para reproducir
            */
            if (musica_actual > 0){
                musica_actual = (musica_actual - 1);
                musica_actual = musica_actual % (longitud_array);


                audio.src = rutas[musica_actual];
                audio.play();
                $("#play").hide();
                $("#pause").show();

                //Actualizamos los datos
                actualizar_info();
            }
        });

        //Repetir la canción en bucle
        $("#repetir").click(function(){

            if (audio.loop == true){

                audio.loop = false;
                $("#repetir").css("color", "black");

            }else{
                audio.loop = true;
                $("#repetir").css("color", "#00CC99");
            }
        });



        //Reproduccion aleatoria
        $("#random").click(function(){

            if (random_activo == false){

                random_activo = true;

                $("#random").css("color", "#00CC99");

            } else{

                random_activo = false;
                $("#random").css("color", "black");
            }

        });


    });
    </script>

</body>
</html>
