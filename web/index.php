<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>RSA Cloud</title>
<link rel="icon"  href="images/logo.ico">
<style>
  div p {
      top: 50%;
      left: 10%;
      text-transform: uppercase;
      font-size: 40px;
      font-family: sans-serif, monospace;
  }

  .principalTitle p {
      color: #fff;
      margin: 20% 0% 0% 5%;
  }
</style>
</head>
<body>
     <?php

        session_start();

        if(!isset($_SESSION["user_id"])){
            include "menu.html";
        } else {
            include "menu_loged.html";
        }
    ?>
    <div class="principalTitle">
        <p>save your files and music in the best cloud server</p>
    </div>

</body>
</html>
