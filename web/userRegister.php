<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Register</title>
    <link rel="icon"  href="images/logo.ico">
    <link rel="stylesheet" href="css/formStyle.css">
    <style>
        /*Estilo y posiciones de formulario*/
        .Contenedor {
          transform: translateX(50%) translateY(12%);
        }
        label {
          color: #fff;
        }
        .error {
          color: red;
        }

        button:hover {
          background: rgba(0, 0, 0, 0.7);
        }
        .Contenedor input, textarea {
          padding-left: 4px;
        }


        @media screen and (max-width: 750px){
          .Contenedor {
              transform: translateX(20%) translateY(12%);
              background: none;
          }

          .Contenedor h1 {
            margin-bottom: 3%;
          }

          form input.rpass {
            margin-bottom: 2%;
          }

          form div.button {
            margin: 0 0 1% 0;
          }
      }

    </style>
</head>

<body>
    <?php

	session_start();
		if(!isset($_SESSION["user_id"])){
		    include "menu.html";
		} else{
		    include "menu_loged.html";
		}
?>
<?php
//Nos conectamos a la base de datos

    $servername = "localhost";
    $username = "root";
    $password = "root123";
    $db = "RSA";
    $table = "users";

    // Create connection
    $conn = new mysqli($servername, $username, $password, $db);

    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

// Creamos las variables con valor vacio
$firstname = $lastname = $email = $user_password = $user_retype_password = "";
//Variables para los errores
$firstnameErr = $lastnameErr = $emailErr = $user_passwordErr = $user_retype_passwordErr ="";

//Creamos una variable para determinar si enviamos o no la información a la base de datos
$enviar_datos = "no";

//Crearemos una variable para contar los erroes a la hora de introducir datos
//Si hay uno o más errores no se enviarán los datos
$errores = 0;



//Comprobamos los datos

if ($_SERVER["REQUEST_METHOD"] == "POST") {

  if (empty($_POST["firstname"])){
    $firstnameErr = "Name is required";
    $errores = $errores + 1;
  } else{
    $firstname = test_input($_POST["firstname"]);
    if (!preg_match("/^([A-Za-zÑñ ]+[áéíóú]?[A-Za-z ]*)$/",$firstname)) {
      $firstnameErr = "Only letters";
      $errores = $errores + 1;
    }
  }


  if (empty($_POST["lastname"])){
    $lastnameErr = "Lastname is required";
    $errores = $errores + 1;

  } else {
    $lastname = test_input($_POST["lastname"]);

    if (!preg_match("/^([A-Za-zÑñ ]+[áéíóú]?[A-Za-z ]*)$/",$lastname)) {
      $lastnameErr = "Only letters";
      $errores = $errores + 1;
    }
  }


  if (empty($_POST["email"])){
    $emailErr = "Email is required";
    $errores = $errores + 1;
  } else {
    $email = test_input($_POST["email"]);

    //Validamos el email
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $emailErr = "Formato de email inválido";
      $errores = $errores + 1;
    }

    //Comprobamos que los valores no se hayan añadido anteriormente
    //Extraemos el email para asegurar que no se repita

    $sql_get_email = "SELECT * FROM $table WHERE Email = '$email'";
    $result = $conn->query($sql_get_email);

    if ($result->num_rows == 1) {

        $errores = $errores + 1;

        while ($row = $result->fetch_assoc()) {

          $emailErr = "Sorry, the email has already been registered";
        }

    }
  }

  //Password

  if (empty($_POST["user_password"])){
      $user_passwordErr = "Password is required";
      $errores = $errores + 1;
    } else{
        $user_password = test_input($_POST["user_password"]);
    }

    if (empty($_POST["user_retype_password"])){
        $user_retype_passwordErr = "Please retype the password";
        $errores = $errores + 1;
    } else{
        $user_retype_password = test_input($_POST["user_retype_password"]);

        if ( $user_retype_password != $user_password){

            $user_retype_passwordErr = "The introduced passwords doesn't match";

            $errores = $errores + 1;
        }

    }

}

//Encriptacion de contraseña
$hashpwd = md5($user_password);


function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}


//Comprobamos si debemos enviar los datos o no
/*
Para que no se nos envíen datos en blanco la primera vez que cargamos la página
comprobamos que no hay errores y que además el usuario ha introducido alguna
información en alguna variable, en este caso comprobamos que la variable
nombre no tenga el valor por defecto, por lo que el usuario habrá intercatuado
*/

if ($errores == 0 && $firstname != ""){
    $enviar_datos = "si";
}


if ( $enviar_datos == "si"){

    //Insertar valores

    $sql_insert_user = "INSERT INTO $table (firstname, lastname, email, pwd )
    VALUES ( '$firstname ', '$lastname' , '$email', '$hashpwd')";

    if ($conn->query($sql_insert_user) === TRUE) {
      echo '<script language="javascript">';
      echo 'alert("Data Sent Successfully")';
      echo '</script>';

    } else {
        echo "Error: " . $sql_insert_user . "<br>" . $conn->error;
    }

    //Extraemos el ID generado

    $sql_get_id = "SELECT * FROM $table WHERE firstname = '$firstname' AND email = '$email'";
    $result = $conn->query($sql_get_id);


    if ($result->num_rows == 1) {

        while ($row = $result->fetch_assoc()) {

		$id_user = $row["id_user"];
		$user_name = $row["firstname"];
        }

        //Creamos una tabla

        $user_table = "music_user_$id_user";

        $sql_create_user_table = "CREATE TABLE $user_table (
          id_music INT AUTO_INCREMENT PRIMARY KEY,
          title VARCHAR(280) NOT NULL,
          artist VARCHAR(280) NOT NULL,
          file_path VARCHAR(280) NOT NULL
          )";

        if ($conn->query($sql_create_user_table) === TRUE) {
            echo '<script language="javascript">';
            echo 'alert("Personal Table created Successfully")';
            echo '</script>';
            // Creamos las variables con valor vacio
            $firstname = $lastname = $email = $user_password = $user_retype_password = "";
            //Variables para los errores
            $firstnameErr = $lastnameErr = $emailErr = $user_passwordErr = $user_retype_passwordErr = "";
		
            session_start();

            $_SESSION["user_id"] = $id_user;
            $_SESSION["user_name"] = $user_name;
            header("location: login.php");

        } else {
            echo "Error creating table: " . $conn->error;
        }


    } else {
        echo "No user ID found";
    }



  //Cerramos conexión
  $conn->close();

}


?>

<div class="Contenedor">
  <h1>Sign Up</h1>
  <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">

    <label for="firstname">Firstname * </label><span class="error"> <?php echo $firstnameErr;?></span>
    <input type="text" name="firstname" value="<?php echo $firstname;?>"><br>

    <label for="lastname">Lastname * </label><span class="error"> <?php echo $lastnameErr;?></span>
    <input type="text" name="lastname" value="<?php echo $lastname;?>"><br>


    <label for="email">Email * </label><span class="error"> <?php echo $emailErr;?></span>
    <input type="text" name="email" value="<?php echo $email;?>"><br>

    <label for="user_password">Password * </label><span class="error"> <?php echo $user_passwordErr;?></span>
    <input type="password" name="user_password" value="<?php echo $user_password;?>"><br>

    <label for="user_retype_password">Retype Password * </label><span class="error"> <?php echo $user_retype_passwordErr;?></span>
    <input class="rpass" type="password" name="user_retype_password" value="<?php echo $user_retype_password;?>"><br>

    <div class="button">
        <button type="submit" name="send" value="Send">Send</button>
    </div>

  </form>

</div>

</body>
</html>
