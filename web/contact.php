<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Contact</title>
    <link rel="icon"  href="images/logo.ico">
    <link rel="stylesheet" href="css/formStyle.css">
    <style>
      .Contenedor input, textarea {
        padding-left: 4px;
      }
    </style>
</head>

<body>
<!--Incluimos el header en esta página.-->
    <?php

        session_start();

        if(!isset($_SESSION["user_id"])){
            include "menu.html";
        } else {
            include "menu_loged.html";
        }
    ?>

<!--Este formulario está hecho para ayudar a los usuarios.-->
<!--Este div contiene todo el formulario y su nombre-->
<div class="Contenedor">
    <h1>Contact us</h1>
    <p>Send us your details and we will contact you!</p>

    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
        <div class="input">
            <input type="name" name="name" required>
            <label>Name</label>
        </div>
        <div class="input">
            <input type="email" name="email" required>
            <label>E-mail</label>
        </div>
        <div class="input">
            <input type="name" name="subject" required>
            <label>Subject</label>
        </div>
        <div class="input">
            <textarea name="msg" rows="4" cols="40" required></textarea>
            <label>Message</label>
        </div>
        <div class="button">
            <button type="submit" name="submit">Submit</button>
        </div>
    </form>
</div>

<!--Aquí recogemos información del usuario y la guardamos en la base de datos.-->
<?php
    /*Definir variables y establecer valores vacíos.*/
    $name = $email = $subject = $message = "";

    /*Asignamos a cada variable su valor correspondiente.*/
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      $name = test_input($_POST["name"]);
      $email = test_input($_POST["email"]);
      $subject = test_input($_POST["subject"]);
      $message = test_input($_POST["msg"]);
    }

    /*Esto evita que los atacantes exploten el código inyectando código HTML o Javascript (ataques de scripts entre sitios) en formularios.*/
    function test_input($data) {
      $data = trim($data);
      $data = stripslashes($data);
      $data = htmlspecialchars($data);
      return $data;
    }

    /*Verificamos que haya datos ingresados ​​antes de enviarlos.*/
    if ($name != "") {
      /*Connection Data*/
      $servername = "localhost";
      $username = "root";
      $password = "root123";
      $dbname = "RSA";

      // Create connection
      $conn = new mysqli($servername, $username, $password, $dbname);
      // Check connection
      if ($conn->connect_error) {
          die("Connection failed: " . $conn->connect_error);
      }

      $sql = "INSERT INTO contact_us (name, email, subject, message)
      VALUES ('$name', '$email', '$subject', '$message')";

      if ($conn->query($sql) === TRUE) {
          echo '<script language="javascript">';
          echo 'alert("message successfully sent")';
          echo '</script>';

      } else {
          echo "Error: " . $sql . "<br>" . $conn->error;
      }

      $conn->close();
    }
?>

</body>
</html>
