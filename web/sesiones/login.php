<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8">
    <title>Login</title>
    <link rel="stylesheet" href="../css/formStyle.css">
    <link rel="icon"  href="../images/logo.ico">
    <style>
        .Contenedor h1 {
          margin-bottom: 10%;
        }

        .Contenedor #email {
          margin-bottom: 15%;
	}

	label{
	    color:#fff;
	}
	.error{
	    color:red;
	}

    </style>
</head>

<body>
<?php include "menuSession.html"; ?>

<?php
    //connection
    $servername = "localhost";
    $username = "root";
    $password = "root123";
    $dbname = "RSA";
    $table = "users";

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    //Variables
    $email = $email_correct = $pwd = "";
    $emailErr = $pwdErr = "";


    if ($_SERVER["REQUEST_METHOD"] == "POST") {

        if (empty($_POST["email"])){ //Comprobamos si está el campo vacío

            $emailErr = " Email is required";

          } else {

                $email = test_input($_POST["email"]);

                //Comprobamos que el email se encuentre en la base de datos

                $sql_get_email = "SELECT * FROM $table WHERE email = '$email'";

                $result = $conn->query($sql_get_email);

                if ($result->num_rows == 0) {

                    $emailErr = " The introduced email is not registered";


                } else {
                    $email_correct = "yes";
                }
        }


        if (empty($_POST["password"])) {

            $pwdErr = " The password is required";

        } else {

            $pwd = test_input($_POST["password"]);

            //Encriptamos la contraseña con MD5
            $hashPwd = md5($pwd);

            if ($email_correct == "yes" ){

                $sql_get_pass = "SELECT * FROM $table WHERE email = '$email' AND pwd = '$hashPwd'";

                $result = $conn->query($sql_get_pass);

                if ($result->num_rows == 0) {

                    $pwdErr = " Incorrect password";

                } else {
                    //Extraemos el ID generado, lo usaremos para recoger la música del usuario

                    $sql_get_id = "SELECT * FROM $table WHERE email = '$email' AND pwd = '$hashPwd'";

                    $result = $conn->query($sql_get_id);

                    if ($result->num_rows == 1) {

                        while ($row = $result->fetch_assoc()) {

                            $id_user = $row["id_user"];
                            $user_name = $row["firstname"];

                        }

                        session_start();

                        $_SESSION["user_id"] = $id_user;
                        $_SESSION["user_name"] = $user_name;

                        header("location:../inicio_public.php");

                    } else {
                        echo "no ID found";
                    }
                }
            }

        }
    }


    //Una Función que elimina todos los caracteres especiales y espacios en blancos etc.

    function test_input($data) {
        $data = trim($data);
        $data = stripcslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

?>

    <div class="Contenedor">
        <h1>Login</h1>
        <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">

	    <label for="email">Email </label><span class="error"><?php echo $emailErr;?></span>
            <div class="input">
                <input id="email" type="text" name="email" value="<?php echo $email;?>">
            </div>

	    <label for="password">Password </label><span class="error"><?php echo $pwdErr;?></span>
	    <div class="input">
                <input type="password" name="password"><br><br>
            </div>

            <div class="button">
                <button type="submit" name="submit">Submit</button>
            </div>

        </form>
    </div>

    <!--
    <script src="js/jquery.js"></script>

    //Archivo de bootstrap
    <script src="js/bootstrap.min.js"></script>
  -->

</body>
</html>
