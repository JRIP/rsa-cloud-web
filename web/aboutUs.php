<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title>About Us</title>
	<link rel="icon"  href="images/logo.ico">
	<link rel="stylesheet" href="css/bootstrap.min.css">

	<style>
		/*Estilo de about us*/
		#titulo {
			position: relative;
			display:flex;
			align-items:center;
			text-align: center;
			height: 100vh;
			background-color: rgba(151,174,136,0.58);
			color: white;
			}

			#contenedor{
				display:block;
				margin:auto;
			}

			#about_title{
			position: relative;
			margin:0;
			margin-bottom:10px;
			font-size: 100px;
			}


			#texto_principal {
				position: relative;
				color: white;
				font-family: arial;
				margin: 5%;
				word-spacing: 1%;
			}

			p {
			color: #fff;
			font-family: arial;
			font-size: 30px;
			padding: 5% 5% 0 5%;
			word-spacing: 1%;
			}

			.row {
				margin: 5% 0 0 0;
			background: rgba(0, 0, 0, 0.5);
			}

			.img_caja1 {
				position: relative;
			width: 100%;
				color: white;
			}

			.img_caja2 {
				position: relative;
			width: 100%;
				color: white;
			}

			.video_caja3 {
				position: relative;
			width: 20%;
			}


	</style>
</head>

<body>
	<!--Incluimos el header en esta página.-->
	<?php

        session_start();

        if(!isset($_SESSION["user_id"])){
            include "menu.html";
        } else {
            include "menu_loged.html";
        }
    ?>

	<div id="titulo">
		<div id="contenedor">
			<h1 id="about_title">About Us</h1>
			<img class="scrolldown.gif" src="images/scrolldown.gif" alt="scrolldown">
		</div>
	</div>
	<div id="texto_principal">
		<h2>On our website we have two built-in services: Stream and Storage. Normally these services are on other platforms separately, here we have them on a single server.</h2>
	</div>

	<div class="container-fluid">

		<div class="row">
			<div class="col-md-6 col-xs-12">
				<p>In the Cloud Streaming service you can enjoy the music that is stored in the private playlist, and in addition, you can also upload your audio files with .mp3 or .mp4 extensions and listen to them whenever you want.</p>
			<p>You can register by clicking <a href="userRegister.php">here</a> or log in by clicking <a href="inicio_public.php">here</a>.</p>
			</div>
			<div  class="col-md-6 col-xs-12">
				<img class="img_caja1" src="images/rsa_streaming.png" alt="Vista web">
			</div>
		</div>

		<div class="row">
			<div class="col-md-6 col-xs-12">
				<p>The owncloud service is the private and secure space to upload your files.
The default password for each user in owncloud is <b>rsacloud123</b>. Once you have logged in you can change your password.
You can also download the owncloud application for mobile.</p>
				<p>Download OwnCloud for Android   <a href="https://drive.google.com/open?id=1Fbshdf-Nmqn9mFj6MJLLWxoQXskcbXzK">Click here</a></p>
			</div>
			<div  class="col-md-6 col-xs-12 ">
				<img class="img_caja2" src="images/rsa_storage.png" alt="Vista web">
			</div>
		</div>

		<div class="row">
			<div class="col-md-6 col-xs-12">
				<p>This is a demo video.</p>
			</div>
				<video controls class="col-md-6 col-xs-12 ">
  			<source class="video_caja3" src="video/videoprueba.mp4" type="video/mp4">
			</video>
		</div>

	</div>
</body>
</html>
