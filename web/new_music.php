<?php

session_start();

// Creamos las variables con valor vacio
$titulo = $autor = "";
//Variables para los errores
$tituloErr = $autorErr = "";

//Creamos una variable para determinar si enviamos o no la información a la base de datos
$enviar_datos = "no";

//Con esta variable determinaremos si se enviarán los datos o no
$uploadOk = 1;


//Comprobamos los datos

if ($_SERVER["REQUEST_METHOD"] == "POST") {


    //TITULO DE LA CANCION
    if (empty($_POST["titulo"])){
        $tituloErr = "Se requiere el título de la canción";
        $uploadOk = 0;
      } else {
        $titulo = test_input($_POST["titulo"]);
        }


    //AUTOR DE LA CANCION
    if (empty($_POST["autor"])){
        $autorErr = "Autor requerido";
        $uploadOk = 0;
  } else{
        $autor = test_input($_POST["autor"]);
    }

}


function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);

    return $data;
}


//ARCHIVO

$target_dir = "audios/";
$target_file = $target_dir . basename($_FILES["archivo"]["name"]);
echo $target_file . "<br>";
$FileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));


// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}

// Allow certain file formats
if($FileType != "mp3" && $FileType != "mp4") {
    echo "Sorry, only MP3 or MP4 files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {

	echo $titulo . "<br>";
	echo $autor . "<br>";
	echo $target_file . "<br>";
	print_r ($_FILES);
	
	if (move_uploaded_file($_FILES["archivo"]["tmp_name"], $target_file)) {
        echo "The file ". basename( $_FILES["archivo"]["name"]). " has been uploaded.";

        //Nos conectamos a la base de datos

        $servername = "localhost";
        $username = "root";
        $password = "root123";
        $db = "RSA";
	$id_usuario = $_SESSION["user_id"];
        $tabla = "music_user_" . $id_usuario;

        // Create connection
        $conn = new mysqli($servername, $username, $password, $db);

        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        echo "Conectado a la base de datos correctamente <br>";

        //Insertar valores

        $sql = "INSERT INTO $tabla ( title, artist, file_path )
        VALUES ('$titulo', '$autor', '$target_file')";

        if ($conn->query($sql) === TRUE) {
            echo "Registros creados correctamente <br>";
            header("location: inicio_private.php");
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }

        //Cerramos conexión
        $conn->close();

    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}

?>
